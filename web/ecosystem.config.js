module.exports = {
  apps: [
    {
      name: 'NuxtWeb',
      exec_mode: 'fork', // 默认fork   cluster（集群）
      instances: 'max', // Or a number of instances
      script: './node_modules/nuxt/bin/nuxt.js',
      args: 'start'
    }
  ]
}