// 开发环境配置
export default () => ({
  mysql: {
    host: '192.168.119.183',
    port: 3306,
    username: 'root',
    password: 'ADMIN@123#admin',
    database: 'nest_cms_api',
    synchronize: false,
    logging: true, // 开启日志记录
  },
  // fastdfs 配置
  fastdfs: {
    host: '192.168.119.126',
    port: 22122,
  },
});
