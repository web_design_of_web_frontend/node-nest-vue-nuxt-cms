import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthService } from '../auth.service';

@Injectable()
export class LocalStrategyUser extends PassportStrategy(Strategy, 'userlocal') {
  constructor(private readonly authService: AuthService) {
    super();
  }

  async validate(username: string, password: string): Promise<any> {
    // 验证用户登录信息
    const user = await this.authService.validateUser(username, password);
    if (!user) {
      // 登录失败
      console.log('登录失败!');
      throw new InternalServerErrorException('用户名或密码错误！');
    }
    return user;
  }
}
