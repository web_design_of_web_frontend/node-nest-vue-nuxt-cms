import { createParamDecorator, ExecutionContext } from '@nestjs/common';

// 导出一个名为UsersToken的参数装饰器
export const UsersToken = createParamDecorator(
  // 参数为字符串和执行上下文
  (data: string, ctx: ExecutionContext) => {
    // 切换到HTTP请求
    const request = ctx.switchToHttp().getRequest();
    // 获取用户
    const user = request.user;

    // 如果data存在，返回user?.[data]；否则返回user
    return data ? user?.[data] : user;
  },
);
