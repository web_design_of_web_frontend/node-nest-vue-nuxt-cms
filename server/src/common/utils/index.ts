import * as sharp from 'sharp';

// 假设你有一个Buffer类型或图片路径  返回图片宽高
export async function getImageSize(
  imageBufferOrPath: Buffer | string,
): Promise<{ width: number; height: number }> {
  try {
    const metadata = await sharp(imageBufferOrPath).metadata();
    return { width: metadata.width, height: metadata.height };
  } catch (error) {
    console.error('Error getting image metadata:', error);
    throw new Error('Failed to get image dimensions');
  }
}
