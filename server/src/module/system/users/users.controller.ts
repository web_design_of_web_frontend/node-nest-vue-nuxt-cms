import {
  Controller,
  Post,
  Body,
  UseGuards,
  Request,
  Get,
  Delete,
  Put,
  Param,
  Query,
  HttpException,
  Logger,
} from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';
import { User } from './users.entity';
import { RemoveUserDto } from './dto/remove-user.dto';
import { RetrieveUserDto } from './dto/retrieve-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { FindUserDto } from './dto/find-user.dto';
import { UpdataUserPasswordDto } from './dto/update-user-password.dto';
import { UpdataUserAvatarDto } from './dto/update-user-avatar.dto';
import { Log } from 'src/common/libs/utils';

@ApiTags('用户')
@Controller('api/users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  // 获取用户信息
  @Get('profile')
  @ApiOperation({ summary: '用户信息' })
  getProfile(@Request() req) {
    return req.user;
  }

  // 增加
  @Post()
  @ApiOperation({ summary: '增加' })
  async create(@Body() createUserDto: CreateUserDto): Promise<User> {
    return await this.usersService.create(createUserDto);
  }

  // 删除
  @Post('delete')
  @ApiOperation({ summary: '删除' })
  async remove(
    @Body() removeUserDto: RemoveUserDto,
    @Request() req,
  ): Promise<any> {
    Log({ req });
    return await this.usersService.delete(removeUserDto);
  }

  // 更新
  @Post('update')
  @ApiOperation({ summary: '更新' })
  async update(@Body() updateUserDto: UpdateUserDto): Promise<any> {
    return await this.usersService.update(updateUserDto);
  }

  // 列表
  @Get('list')
  @ApiOperation({ summary: '列表' })
  async findAllFE(@Query() query: FindUserDto): Promise<User> {
    return await this.usersService.findAll(query);
  }

  // 根据 id 查找
  @Get(':id')
  @ApiOperation({ summary: '根据 id 查找' })
  async findOneById(@Param() params: RetrieveUserDto): Promise<any> {
    return await this.usersService.findOneById(params.id);
  }

  // 根据 id 更新密码
  @Post('password')
  @ApiOperation({ summary: '更新密码' })
  async updatePassword(
    @Body() updataUserPasswordDto: UpdataUserPasswordDto,
  ): Promise<any> {
    return await this.usersService.updatePassword(updataUserPasswordDto);
  }

  // 根据 id 重置密码
  @Post('password/reset/:id')
  @ApiOperation({ summary: '重置密码' })
  async resetPassword(
    @Param() params: RetrieveUserDto,
    @Body() password: string,
  ): Promise<any> {
    return await this.usersService.resetPassword(params, password);
  }

  // 根据 id 设置头像
  @ApiOperation({ summary: '设置头像' })
  @Post('updateAvatar')
  async updateAvatar(
    @Body() updateUserAvatar: UpdataUserAvatarDto,
  ): Promise<any> {
    return await this.usersService.updateAvatar(updateUserAvatar);
  }

  // 数量
  @Get('list/count')
  @ApiOperation({ summary: '用户数量' })
  async getCount() {
    return await this.usersService.getCount();
  }
}
