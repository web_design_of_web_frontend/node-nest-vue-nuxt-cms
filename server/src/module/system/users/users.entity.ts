import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('user')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  password: string;

  @Column({
    nullable: true,
    comment: '头像',
  })
  avatar: string;

  @Column({
    nullable: true,
    comment: '邮箱',
  })
  email: string;

  @Column({
    nullable: true,
    comment: '手机号',
  })
  phone: string;

  @Column('simple-array', {
    nullable: true,
    comment: '角色',
  })
  roles: string[];

  @Column('mediumtext', {
    nullable: true,
    comment: '描述',
  })
  intro: string;

  @Column()
  status: boolean;

  @Column({
    select: false,
  })
  createdAt: Date;

  @Column()
  updatedAt: Date;
}
