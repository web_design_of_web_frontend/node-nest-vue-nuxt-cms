import { Controller, Get, Post, Body, Query, Param } from '@nestjs/common';
import { AnSettingService } from './an-setting.service';
import { CreateAnSettingDto } from './dto/create-an-setting.dto';
import { UpdateAnSettingDto } from './dto/update-an-setting.dto';
import { ApiTags } from '@nestjs/swagger';
import { Public } from 'src/common/auth/decorators/public.decorator';

@ApiTags('配置表')
@Controller('api/setting')
export class AnSettingController {
  constructor(private readonly anSettingService: AnSettingService) {}

  @Post('add')
  create(@Body() createAnSettingDto: CreateAnSettingDto) {
    return this.anSettingService.create(createAnSettingDto);
  }

  @Public()
  @Get()
  findAll(@Query() query: any) {
    return this.anSettingService.findAll(query);
  }

  @Public()
  @Get('info')
  findOne(@Query('code') code: string) {
    return this.anSettingService.findOne(code);
  }

  @Post('update')
  update(@Body() updateAnSettingDto: UpdateAnSettingDto) {
    return this.anSettingService.update(updateAnSettingDto);
  }

  @Post('delete')
  remove(@Query('id') id: string) {
    return this.anSettingService.remove(+id);
  }
}
