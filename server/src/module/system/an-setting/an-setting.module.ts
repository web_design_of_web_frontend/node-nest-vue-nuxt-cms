import { Module } from '@nestjs/common';
import { AnSettingService } from './an-setting.service';
import { AnSettingController } from './an-setting.controller';
import { AnSetting } from './entities/an-setting.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([AnSetting])],
  controllers: [AnSettingController],
  providers: [AnSettingService],
})
export class AnSettingModule {}
