import { PartialType } from '@nestjs/swagger';
import { CreateAnSettingDto } from './create-an-setting.dto';

export class UpdateAnSettingDto extends PartialType(CreateAnSettingDto) {}
