import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('an_setting')
export class AnSetting {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'varchar',
    length: 20,
    comment: '关键字段',
  })
  code: string;

  @Column({
    type: 'varchar',
    length: 2000,
    nullable: true,
    comment: '值',
  })
  value: string;

  @Column('varchar', {
    length: 500,
    nullable: true,
    comment: '备注',
  })
  description: string;
}
