import { Injectable } from '@nestjs/common';
import { CreateAnSettingDto } from './dto/create-an-setting.dto';
import { UpdateAnSettingDto } from './dto/update-an-setting.dto';
import { AnSetting } from './entities/an-setting.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AnSettingService {
  constructor(
    @InjectRepository(AnSetting)
    private anSettingRepository: Repository<AnSetting>,
  ) {}

  create(createAnSettingDto: CreateAnSettingDto) {
    return this.anSettingRepository.save(createAnSettingDto);
  }

  // 查询列表
  async findAll(query: any) {
    const { page = 1, pageSize = 10 } = query;
    let qb = this.anSettingRepository.createQueryBuilder('anSetting');
    qb = qb.skip((page - 1) * pageSize).take(pageSize);
    const [items, totalItemsCount] = await qb.getManyAndCount();
    return {
      total: totalItemsCount,
      data: items,
    };
  }

  findOneByID(id: number) {
    return this.anSettingRepository.findOneBy({ id: id });
  }

  findOne(code: string) {
    return this.anSettingRepository.findOne({
      where: {
        code: code,
      },
    });
  }

  update(updateAnSettingDto: UpdateAnSettingDto) {
    const id = updateAnSettingDto.id;
    return this.anSettingRepository.update(id, updateAnSettingDto);
  }

  remove(id: number) {
    return this.anSettingRepository.delete(id);
  }
}
