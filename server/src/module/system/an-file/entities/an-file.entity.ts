import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('an_file')
export class AnFile {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column('varchar', { length: 500 })
  path: string;

  @Column({
    default: 0,
    nullable: true,
  })
  size: number;

  @Column('varchar', { length: 1000, nullable: true })
  description: string;

  @Column({
    default: 0,
    nullable: true,
    comment: '0文件  1图片',
  })
  type: number;

  @Column({ nullable: true })
  tag: string;

  @CreateDateColumn()
  createdTime: Date;
}
