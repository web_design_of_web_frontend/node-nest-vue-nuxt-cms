import { Module } from '@nestjs/common';
import { AnFileService } from './an-file.service';
import { AnFileController } from './an-file.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnFile } from './entities/an-file.entity';
import { FastDFSService } from 'src/common/core/fastdfs/fastdfs.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [ConfigModule, TypeOrmModule.forFeature([AnFile])],
  controllers: [AnFileController],
  providers: [AnFileService, FastDFSService],
  exports: [AnFileService],
})
export class AnFileModule {}
