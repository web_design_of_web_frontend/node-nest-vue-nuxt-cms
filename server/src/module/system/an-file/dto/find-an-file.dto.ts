import { ApiProperty } from '@nestjs/swagger';
export class FindAnFileDto {
  @ApiProperty({
    required: false,
  })
  startTime: string;

  @ApiProperty({
    required: false,
  })
  endTime: string;

  @ApiProperty()
  page: number;

  @ApiProperty()
  pageSize: number;
}
