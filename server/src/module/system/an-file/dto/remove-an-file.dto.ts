import { IsArray } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class RemoveAnFileDto {
  @ApiProperty()
  @IsArray()
  ids: [];
}
