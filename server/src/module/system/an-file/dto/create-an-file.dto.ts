import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
export class CreateAnFileDto {
  id: string;

  @IsString()
  @IsNotEmpty({ message: '名称不能为空' })
  name: string;

  @IsString()
  @IsNotEmpty()
  path: string;

  @IsNumber()
  size: number;

  @IsString()
  description: string;

  @IsNumber()
  type: number;

  @IsString()
  tag: string;

  createdTime: Date;
}
