import { PartialType } from '@nestjs/swagger';
import { CreateAnFileDto } from './create-an-file.dto';

export class UpdateAnFileDto extends PartialType(CreateAnFileDto) {}
