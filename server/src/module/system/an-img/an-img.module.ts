import { Module } from '@nestjs/common';
import { AnImgService } from './an-img.service';
import { AnImgController } from './an-img.controller';
import { AnFileModule } from '../an-file/an-file.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnImg } from './entities/an-img.entity';

import { FastDFSService } from 'src/common/core/fastdfs/fastdfs.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [AnFileModule, ConfigModule, TypeOrmModule.forFeature([AnImg])],
  controllers: [AnImgController],
  providers: [AnImgService, FastDFSService],
})
export class AnImgModule {}
