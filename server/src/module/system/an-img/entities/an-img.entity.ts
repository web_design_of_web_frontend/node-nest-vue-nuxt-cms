import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('an_img')
export class AnImg {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'varchar',
    length: 200,
    comment: '图片名称',
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 500,
    nullable: true,
    comment: '图片路径',
  })
  url: string;

  @Column({
    type: 'varchar',
    length: 500,
    nullable: true,
    comment: '缩略图路径',
  })
  thumbnail: string;

  @Column({
    type: 'bigint',
    default: 0,
    nullable: true,
    comment: '大小',
  })
  size: number;

  @Column({
    nullable: true,
    comment: '图片尺寸 宽X高 单位px',
  })
  scale: string;

  @Column('varchar', {
    length: 1000,
    nullable: true,
    comment: '图片描述，备注',
  })
  content: string;

  @Column({
    default: 0,
    nullable: true,
    comment: '类型',
  })
  type: number;

  @Column({ nullable: true, comment: '标签' })
  tag: string;

  @Column({ nullable: true, comment: '上传人' })
  userId: string;

  @Column({ nullable: true, comment: '文件ID' })
  fileId: string;

  @CreateDateColumn()
  createdTime: Date;
}
