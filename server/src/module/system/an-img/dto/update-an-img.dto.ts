import { PartialType } from '@nestjs/swagger';
import { CreateAnImgDto } from './create-an-img.dto';

export class UpdateAnImgDto extends PartialType(CreateAnImgDto) {}
