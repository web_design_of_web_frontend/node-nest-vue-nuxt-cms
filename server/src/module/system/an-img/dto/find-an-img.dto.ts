import { ApiProperty } from '@nestjs/swagger';
export class FindAnImgDto {
  @ApiProperty({
    required: false,
  })
  startTime: string;

  @ApiProperty({
    required: false,
  })
  endTime: string;

  @ApiProperty()
  page: number;

  @ApiProperty()
  pageSize: number;
}
