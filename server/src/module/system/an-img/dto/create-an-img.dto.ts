import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsString,
  isNumber,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class CreateAnImgDto {
  id: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  url: string;

  @ApiProperty({ description: '缩略图地址' })
  @IsString()
  thumbnail: string;

  @ApiProperty({ description: '缩略尺寸' })
  @IsNumber()
  scaleSize: number;

  @IsNumber()
  size: number;

  @ApiProperty({ description: '图片尺寸 宽X高 单位px', example: '200x200' })
  @IsString()
  scale: string;

  @IsString()
  content: string;

  @ApiProperty({ description: '类型  0默认相册', example: '0' })
  @IsNumber()
  type: number;

  @ApiProperty({ description: '标签', example: '头像' })
  @IsString()
  tag: string;

  @IsString()
  userId: string;

  @IsString()
  fileId: string;
}
