import { PartialType } from '@nestjs/swagger';
import { CreateAnVideoDto } from './create-an-video.dto';

export class UpdateAnVideoDto extends PartialType(CreateAnVideoDto) {}
