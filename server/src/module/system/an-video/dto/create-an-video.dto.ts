import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateAnVideoDto {
  @ApiProperty()
  @IsString()
  webId: string;

  @ApiProperty({ description: '视频名称' })
  @IsString()
  name: string;

  @ApiProperty({ description: '封面图' })
  @IsString()
  coverImage: string;

  @ApiProperty({ description: '缩略图' })
  @IsString()
  thumbnail: string;

  @IsNumber()
  type: number;

  @ApiProperty({ description: '时长' })
  @IsString()
  videoLength: number;

  @ApiProperty({ description: '标签' })
  @IsString()
  tag: string;

  @IsNumber()
  status: number;

  @ApiProperty({ description: '简介' })
  @IsString()
  description: string;

  @ApiProperty({ description: '剧集 数组字段' })
  urls: string;
}
