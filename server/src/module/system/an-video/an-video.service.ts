import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAnVideoDto } from './dto/create-an-video.dto';
import { UpdateAnVideoDto } from './dto/update-an-video.dto';
import { AnVideo } from './entities/an-video.entity';
import { AnVideoUrl } from './entities/an-video-url.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AnVideoService {
  constructor(
    @InjectRepository(AnVideo)
    private anVideoRepository: Repository<AnVideo>,

    @InjectRepository(AnVideoUrl)
    private anVideoUrlRepository: Repository<AnVideoUrl>,
  ) {}

  // 新增
  async create(createAnVideoDto: CreateAnVideoDto) {
    const newAnVideo = new AnVideo();

    Object.assign(newAnVideo, createAnVideoDto);
    newAnVideo.urls = JSON.parse(createAnVideoDto.urls) || [];

    const anvideo = await this.anVideoRepository.save(newAnVideo);

    const promises = newAnVideo.urls.map(async (item) => {
      const newAnVideoUrl = new AnVideoUrl();
      Object.assign(newAnVideoUrl, item);
      newAnVideoUrl.anVideo = anvideo;
      await this.anVideoUrlRepository.save(newAnVideoUrl);
    });

    await Promise.all(promises);
    return anvideo;
  }

  // 列表查询
  async findAll(query: any) {
    const { page = 1, pageSize = 10, name, type, status } = query;
    let qb = this.anVideoRepository.createQueryBuilder('anVideo');

    if (name) {
      qb = qb.where('(anVideo.name LIKE :name OR anVideo.tags LIKE :tags)', {
        name: `%${name}%`,
        tags: `%${name}%`,
      });
    }
    if (type) {
      qb.andWhere('anVideo.type = :type', { type: +type });
    }
    if (status) {
      qb.andWhere('anVideo.status = :status', { status: +status });
    }
    qb = qb.skip((page - 1) * pageSize).take(pageSize);
    // 添加按 updateTime 倒序排序
    qb.orderBy('anVideo.updateTime', 'DESC');
    const [items, totalItemsCount] = await qb.getManyAndCount();
    return {
      total: totalItemsCount,
      data: items,
    };
  }

  // 查询详情
  async findOne(id: number) {
    try {
      const newAnVideo = await this.anVideoRepository.findOneBy({ id: id });
      newAnVideo.urls = await this.anVideoUrlRepository.find({
        where: {
          videoId: newAnVideo.id,
        },
      });
      return {
        ...newAnVideo,
        createTime: newAnVideo.createTime.toLocaleString(),
        updateTime: newAnVideo.updateTime.toLocaleString(),
      };
    } catch (error) {
      throw new NotFoundException(`找不到数据`);
    }
  }

  // 更新
  async update(id: number, updateAnVideoDto: UpdateAnVideoDto) {
    // 解析JSON格式的urls
    const urlsArray = updateAnVideoDto.urls
      ? JSON.parse(updateAnVideoDto.urls)
      : [];

    const anVideo = await this.anVideoRepository.findOneBy({ id: id });

    if (!anVideo) {
      throw new NotFoundException(`找不到数据`);
    }
    // 更新主表数据，排除urls属性
    const mainEntityData = { ...updateAnVideoDto };
    delete mainEntityData.urls;
    Object.assign(anVideo, mainEntityData);

    // 更新从表数据（如果有）
    if (urlsArray.length) {
      // 清空原有的从表数据
      await this.anVideoUrlRepository.delete({ videoId: id });
      // 保存新的从表数据
      const urlPromises = urlsArray.map(async (urlItem) => {
        const newAnVideoUrl = new AnVideoUrl();
        newAnVideoUrl.videoId = anVideo.id;
        Object.assign(newAnVideoUrl, urlItem);
        await this.anVideoUrlRepository.save(newAnVideoUrl);
      });
      await Promise.all(urlPromises);
    }

    // 保存主表数据
    anVideo.updateTime = new Date();
    await this.anVideoRepository.update(id, anVideo);
    return 'ok';
  }

  // 删除
  async remove(ids: string[]) {
    ids.forEach(async (id) => {
      // 清空原有的从表数据
      await this.anVideoUrlRepository.delete({ videoId: +id });
    });
    //清空主表数据
    await this.anVideoRepository.delete(ids);
    return 'ok';
  }
}
