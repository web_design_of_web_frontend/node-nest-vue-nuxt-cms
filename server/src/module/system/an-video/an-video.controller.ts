import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
  HttpException,
} from '@nestjs/common';
import { AnVideoService } from './an-video.service';
import { CreateAnVideoDto } from './dto/create-an-video.dto';
import { UpdateAnVideoDto } from './dto/update-an-video.dto';
import { ApiTags } from '@nestjs/swagger';
import { Public } from 'src/common/auth/decorators/public.decorator';

@ApiTags('视频模块')
@Controller('api/video')
export class AnVideoController {
  constructor(private readonly anVideoService: AnVideoService) {}

  @Post('add')
  create(@Body() createAnVideoDto: CreateAnVideoDto) {
    return this.anVideoService.create(createAnVideoDto);
  }

  @Get('list')
  @Public()
  findAll(@Query() query: any) {
    return this.anVideoService.findAll(query);
  }

  // 获取单个视频信息
  @Get('info')
  @Public()
  findOne(@Query('id') id: string) {
    return this.anVideoService.findOne(+id);
  }

  @Post('update')
  update(@Body('id') id: string, @Body() updateAnVideoDto: UpdateAnVideoDto) {
    return this.anVideoService.update(+id, updateAnVideoDto);
  }

  @Post('delete')
  remove(@Body('ids') ids: string) {
    if (!ids) {
      throw new HttpException('参数错误', 1);
    }
    const aryIds = ids.split(',');
    return this.anVideoService.remove(aryIds);
  }
}
