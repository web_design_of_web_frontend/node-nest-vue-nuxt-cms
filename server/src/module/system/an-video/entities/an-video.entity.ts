import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import { AnVideoUrl } from './an-video-url.entity';

@Entity('an_video')
export class AnVideo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'char',
    length: 36,
    comment: '外部主键',
  })
  webId: string;

  @Column({
    type: 'varchar',
    length: 255,
    comment: '标题',
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 500,
    nullable: true,
    comment: '封面图',
  })
  coverImage: string;

  @Column({
    type: 'varchar',
    length: 500,
    nullable: true,
    comment: '封面缩略图',
  })
  thumbnail: string;

  @Column({
    type: 'int',
    nullable: true,
    comment: '类型',
  })
  type: number;

  @Column({
    type: 'int',
    nullable: true,
    comment: '时长',
  })
  videoLength: number;

  @Column({
    type: 'varchar',
    length: 255,
    comment: '标签',
  })
  tags: string;

  @Column({
    type: 'int',
    default: 0,
    comment: '状态:0草稿，1发布',
  })
  status: number;

  @Column({
    type: 'mediumtext',
    comment: '简介',
  })
  description: string;

  @CreateDateColumn()
  createTime: Date;

  @UpdateDateColumn()
  updateTime: Date;

  @OneToMany(() => AnVideoUrl, (anVideoUrl) => anVideoUrl.anVideo, {
    cascade: true,
  })
  urls: AnVideoUrl[];
}
