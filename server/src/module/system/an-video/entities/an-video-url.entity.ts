import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { AnVideo } from './an-video.entity';

@Entity('an_video_url')
export class AnVideoUrl {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'varchar',
    length: 255,
    comment: '标题',
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 500,
    nullable: true,
    comment: '封面图',
  })
  coverImgUrl: string;

  @Column({
    name: 'videoId',
    type: 'int',
    comment: '视频ID',
  })
  videoId: number;

  @Column({
    type: 'varchar',
    length: 2000,
    comment: '视频地址',
  })
  videoUrl: string;

  @ManyToOne(() => AnVideo, (anVideo) => anVideo.urls, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'videoId' })
  anVideo: AnVideo;
}
