import { Module } from '@nestjs/common';
import { AnVideoService } from './an-video.service';
import { AnVideoController } from './an-video.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnVideo } from './entities/an-video.entity';
import { AnVideoUrl } from './entities/an-video-url.entity';

@Module({
  imports: [TypeOrmModule.forFeature([AnVideo, AnVideoUrl])],
  controllers: [AnVideoController],
  providers: [AnVideoService],
})
export class AnVideoModule {}
