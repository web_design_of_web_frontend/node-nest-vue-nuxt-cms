import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './module/system/users/users.module';
import { ProductsModule } from './module/system/products/product.module';
import { ProductCategoriesModule } from './module/system/product-categories/product-categories.module';
import { AboutModule } from './module/system/about/about.module';
import { HomeModule } from './module/system/home/home.module';
import { BannerModule } from './module/system/banner/banner.module';
import { UploadModule } from './module/upload/upload.module';
import { AuthModule } from './common/auth/auth.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { Log4jsModule } from './common/libs/log4js/';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AnFileModule } from './module/system/an-file/an-file.module';
import { AnImgModule } from './module/system/an-img/an-img.module';
import { AnSettingModule } from './module/system/an-setting/an-setting.module';
import { AnVideoModule } from './module/system/an-video/an-video.module';
import { DictModule } from './module/system/dict/dict.module';
import configDefault from '../config/config.default';

@Module({
  imports: [
    // 配置模块
    ConfigModule.forRoot({
      load: [configDefault],
    }),
    // TypeOrm模块
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get('mysql.host'),
        port: +configService.get('mysql.port'),
        username: configService.get('mysql.username'),
        password: configService.get('mysql.password'),
        database: configService.get('mysql.database'),
        synchronize: configService.get('mysql.synchronize'),
        logging: configService.get('mysql.logging'),
        autoLoadEntities: true,
        keepConnectionAlive: true,
      }),
    }),
    // 静态资源模块
    ServeStaticModule.forRoot(
      {
        rootPath: join(__dirname, '..', 'public'),
        serveRoot: '/',
      },
      {
        rootPath: join(__dirname, '..', 'uploads'),
        serveRoot: '/uploads',
      },
    ),
    // 日志模块
    Log4jsModule.forRoot(),
    // 业务模块...
    UsersModule,
    ProductsModule,
    ProductCategoriesModule,
    AboutModule,
    HomeModule,
    BannerModule,
    UploadModule,
    AuthModule,
    AnFileModule,
    AnImgModule,
    AnSettingModule,
    AnVideoModule,
    DictModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
